import React from "react";
import styles from "./page.module.css";
import Image from "next/image";
import Button from "@/components/Button/Button";

const About = () => {
  return (
    <section className={styles.container}>
      <div className={styles.imgContainer}>
        <Image
          src="https://images.pexels.com/photos/3194521/pexels-photo-3194521.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
          alt=""
          fill={true}
          className={styles.img}
        />
        <div className={styles.imgText}>
          <h1 className={styles.imgTitle}>Digital Storytellers</h1>
          <h2 className={styles.imgDesc}>
            Handcrafting award wining digital experiences
          </h2>
        </div>
      </div>
      <div className={styles.textContainer}>
        <div className={styles.item}>
          <h3 className={styles.title}>Who are We?</h3>
          <p className={styles.desc}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore
            deleniti sint repellat fugiat vero possimus dolor molestiae
            doloribus, quaerat ut cum accusamus obcaecati. Voluptatum eius
            voluptate, quas amet corporis provident tenetur laborum labore,
            pariatur sequi aut ut veritatis nihil! Numquam aspernatur, a quasi
            cupiditate doloribus vitae fugiat atque saepe exercitationem.
            <br />
            <br />
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius optio
            voluptatum atque nulla tempora totam unde ad vitae explicabo?
            Laudantium quidem ducimus accusantium nisi, alias, exercitationem
            eligendi tempora dolore hic quisquam consequuntur fuga, optio porro
            pariatur soluta eius nostrum. Sint eum, nam repellendus amet cum
            harum doloremque ab a atque!
          </p>
        </div>{" "}
        <div className={styles.item}>
          <h3 className={styles.title}>What are Do?</h3>
          <p className={styles.desc}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore
            deleniti sint repellat fugiat vero possimus dolor molestiae
            doloribus, quaerat ut cum accusamus obcaecati. Voluptatum eius
            voluptate, quas amet corporis provident tenetur laborum labore,
            pariatur sequi aut ut veritatis nihil! Numquam aspernatur, a quasi
            cupiditate doloribus vitae fugiat atque saepe exercitationem.
            <br />
            <br /> - Dynamic Websites
            <br />
            <br /> - Fast and Handy
            <br />
            <br /> - Mobile Apps
          </p>
          <Button text="Contact" url="/contact" />
        </div>
        <div className={styles.item}></div>
      </div>
    </section>
  );
};

export default About;
