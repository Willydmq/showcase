import Image from "next/image";
import styles from "./footer.module.css";

const Footer = () => {
  return (
    <footer className={styles.container}>
      <div> © 2023 Lamamia. All rights reserved</div>
      <div className={styles.social}>
        <Image
          src="/1.png"
          alt="Lama dev Facebook Account"
          width={15}
          height={15}
          className={styles.icon}
        />
        <Image
          src="/2.png"
          alt="Lama dev Instagram Account"
          width={15}
          height={15}
          className={styles.icon}
        />
        <Image
          src="/3.png"
          alt="Lama dev Twitter Account"
          width={15}
          height={15}
          className={styles.icon}
        />
        <Image
          src="/4.png"
          alt="Lama dev Youtube Account"
          width={15}
          height={15}
          className={styles.icon}
        />
      </div>
    </footer>
  );
};

export default Footer;
