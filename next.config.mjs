/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ["images.pexels.com", "example.com"],
  },
  distDir: "out",
};

export default nextConfig;
