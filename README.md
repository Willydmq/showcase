# Nombre del Proyecto 📋

"**Showcase:** es proporcionar una plataforma donde los desarrolladores, diseñadores y otros profesionales puedan mostrar su trabajo y habilidades". 😀

<div style="text-align: center; padding: 10px; display:flex flex-direction:column">
    <h1 style="font-size:25px; text-decoration-line: underline;">Version Escritorio 💻</h1>
    <div style="display:flex; flex-wrap: wrap; gap:5px; justify-content: center;">
      <img src="/public/showcase.png" width="300px" style="border: 3px solid #53c28b;">
      <img src="/public/showcase_1.png" width="300px" style="border: 3px solid #53c28b;">
      <img src="/public/showcase_2.png" width="300px" style="border: 3px solid #53c28b;">
    </div>
</div>

# Link Proyecto

<div style="display: flex; flex-direction: column; align-items: center;">
    <img src="/public/portfolio.png" width="50px">
    <a style="color: blue; font-size: 20px; display: block; text-align: center;" href="https://potterplayground.pages.dev/" target="_blank">Showcase</a>
</div>

## Instalación ⚙️

1. Asegúrate de tener instalado Node.js y Yarn en tu computadora. Si no lo tienes instalado, puedes descargar e instalar **Node.js y Yarn** desde el sitio web oficial.

2. Descarga o clona el repositorio del proyecto desde GitHub.

3. Ejecuta el comando yarn install para instalar todas las dependencias necesarias del proyecto, que están especificadas en tu archivo `package.json`.

4. Ejecutar el comando yarn install instalará todas las dependencias necesarias del proyecto, incluyendo:

   - ![NPM](https://img.shields.io/badge/bcryptjs-NPM-red) <a href="https://www.npmjs.com/package/bcryptjs">bcryptjs</a>
   - ![NPM](https://img.shields.io/badge/mongoose-NPM-blue) <a href="https://www.npmjs.com/package/mongoose">mongoose</a>
   - ![NPM](https://img.shields.io/badge/next-NPM-green) <a href="https://www.npmjs.com/package/next">next</a>
   - ![NPM](https://img.shields.io/badge/nextauth-NPM-orange) <a href="https://www.npmjs.com/package/next-auth">next-auth</a>
   - ![NPM](https://img.shields.io/badge/react-NPM-white) <a href="https://www.npmjs.com/package/react">react</a>
   - ![NPM](https://img.shields.io/badge/reactDom-NPM-violet) <a href="https://www.npmjs.com/package/react-dom">react-dom</a>
   - ![NPM](https://img.shields.io/badge/swr-NPM-limon) <a href="https://www.npmjs.com/package/swr">swr</a>

   ```
   yarn install
   ```

5. Una vez completados los pasos anteriores, ejecutar el comando npm run dev para iniciar la aplicación.
   ```
   yarn run dev
   ```
6. Acceder a la URL http://localhost:3000 en un navegador web para ver la aplicación en funcionamiento.

### Requisitos 📄

1. Conocimientos en HTML, CSS, JavaScript, **Next.js, MongoDB y Google API**.

2. Conocimientos de Git: La aplicación utiliza Git para el control de versiones, por lo que necesitará conocimientos básicos de Git para clonar el repositorio del proyecto, crear ramas, fusionar cambios y enviar solicitudes de extracción.

## Uso 💪

La aplicación **“Showcase”** que hemos desarrollado puede ser utilizada por profesionales que buscan mostrar sus proyectos y habilidades a posibles empleadores o clientes. Los usuarios pueden cargar sus proyectos, describir sus habilidades y proporcionar información de contacto para que los visitantes puedan ponerse en contacto con ellos.

## Construido con 🛠️

<div style="text-align: center; padding: 10px;">
    <img src="/public/nextjs.png" width="100px">
    <img src="/public/mongodb.png" width="100px">
    <img src="/public/google.png" width="100px">
</div>

## Deployment 🚀

La arquitectura de nuestra aplicación consta de dos partes principales: el frontend y el backend, ambos construidos con **Next.js**. El frontend utiliza un enfoque de arquitectura de componentes para organizar y reutilizar el código. Para la autenticación de usuarios, se utiliza la **API de Google**, lo que permite a los usuarios iniciar sesión con sus cuentas de Google.El backend también está construido con **Next.js y se conecta a una base de datos MongoDB** para almacenar y recuperar datos. Utilizamos **bcrypt** para cifrar las contraseñas de los usuarios y garantizar la seguridad de sus datos.

## Autores ✒️

- **William Maldonado** - _Challenge: Next.js 13 Full Stack App Using App Router_ - [Willydmq](https://gitlab.com/Willydmq)

## Expresiones de Gratitud

- Quiero expresar mi más profundo agradecimiento a **[Lama Dev](https://www.youtube.com/watch?v=VE8BkImUciY)**. por su increíble video de YouTube del Challenge. Sus enseñanzas fueron fundamentales para la realización de este proyecto. 🤓.
- La claridad de sus explicaciones y la profundidad de sus conocimientos me permitieron adquirir nuevas habilidades y aplicarlas en la creación de esta aplicación. **[GitHub](https://github.com/safak/nextjs-tutorial)**. 📢.
- Gracias, **[Lama Dev](https://github.com/safak)**!, pude transformar una idea en una aplicación funcional y estoy emocionado por las futuras oportunidades de aprendizaje que sus tutoriales brindarán. 🌟.

---

⌨️ con ❤️ por [William Maldonado](https://gitlab.com/Willydmq) 😊

---
