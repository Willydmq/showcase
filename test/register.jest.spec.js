import { test, vi, expect } from "vitest";
import Category from "../src/app/portfolio/[category]/page.jsx";

test("Category component renders data correctly", () => {
  const mockData = {
    category: "testCategory",
    items: [
      {
        id: 1,
        title: "Test Item 1",
        desc: "This is a test description",
        image: "testImage.jpg",
      },
      {
        id: 2,
        title: "Test Item 2",
        desc: "This is another test description",
        image: "testImage2.jpg",
      },
    ],
  };

  // Create a mock of the useRouter hook
  const mockUseRouter = vi.fn().mockReturnValue({
    query: {
      category: mockData.category,
    },
  });

  // Mock the useSWR hook
  const mockUseSWR = vi.fn().mockReturnValue({
    data: mockData,
    isLoading: false,
  });

  // Mock the useState hook
  const mockUseState = vi.fn();

  // Mock the useEffect hook
  const mockUseEffect = vi.fn();

  // Mock the useRef hook
  const mockUseRef = vi.fn().mockReturnValue({
    current: null,
  });

  // Mock the useContext hook
  const mockUseContext = vi.fn().mockReturnValue({
    theme: "light",
  });
});
