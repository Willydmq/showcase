import { test, vi, expect } from "vitest";
import { getData } from "../src/app/blog/page";

// Single test case using Vitest's `test` function
test("getData fetches data successfully", async () => {
  // Mock fetch behavior to return successful data
  global.fetch = vi.fn(() =>
    Promise.resolve({
      ok: true,
      json: () =>
        Promise.resolve([
          {
            id: 1,
            title: "Test Post",
            desc: "Test Description",
            img: "/test.jpg",
            _id: "1",
          },
        ]),
    })
  );

  // Call the getData function
  const data = await getData();

  // Assert that the fetched data matches the expected response
  expect(data).toEqual([
    {
      id: 1,
      title: "Test Post",
      desc: "Test Description",
      img: "/test.jpg",
      _id: "1",
    },
  ]);
});
