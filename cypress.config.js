module.exports = {
  video: false,
  e2e: {
    setupNodeEvents: (on, config) => {
      // Configuración de tareas y complementos
    },
    defaultCommandTimeout: 10000,
    viewportHeight: 1080,
    viewportWidth: 1920,
  },
};
