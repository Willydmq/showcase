/* global cy */

describe("Blog", function () {
  it("frontpage can be opened", function () {
    cy.visit("http://localhost:3000/blog");
    cy.url().should("include", "/blog");
    cy.contains("Test");
    cy.contains("Desc");
  });
});
