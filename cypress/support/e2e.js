// cypress/support/e2e.js

// Comando personalizado para verificar el contenido de un artículo
Cypress.Commands.add("verifyArticleContent", ($article) => {
  cy.wrap($article).find("h2").should("be.visible");
  cy.wrap($article).find("p").should("be.visible");
  cy.wrap($article).find("a").should("be.visible");
});

// Comando personalizado para navegar a una página de blog individual
Cypress.Commands.add("navigateToBlogPost", (postTitle) => {
  cy.get("article").contains("h2", postTitle).find("a").click();
});
